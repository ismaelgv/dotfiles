-- NeoVim key mapping configuration
local fun = require("functions")
local opts = { noremap = true, silent = true }

-- Navigation
vim.keymap.set("n", "gb", '<cmd>bnext<CR>')
vim.keymap.set("n", "gB", '<cmd>bprevious<CR>')
vim.keymap.set('n', '<leader>bd', '<cmd>bdelete<CR>', opts)

vim.keymap.set("n", "<leader>gc", '<cmd>cnext<CR>')
vim.keymap.set("n", "<leader>gC", '<cmd>cprevious<CR>')
vim.keymap.set("n", "gl", '<cmd>lnext<CR>')
vim.keymap.set("n", "gL", '<cmd>lprevious<CR>')

-- Terminal
vim.keymap.set("t", "<Esc>", [[<C-\><C-n>]])
-- Spelling
vim.keymap.set("n", "<F3>", fun.toggle_spell, opts)
-- Highlight Cursor Line
vim.keymap.set("n", "<F9>", fun.toggle_cursor_line, opts)
-- Color column
vim.keymap.set("n", "<F10>", fun.toggle_color_column, opts)
-- Toggle Line Number
vim.keymap.set("n", "<F12>", fun.toggle_line_number, opts)

-- LSP
-- See `:help vim.lsp.*` for documentation on any of the below functions
vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
vim.keymap.set('n', 'gD', vim.lsp.buf.type_definition, opts)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
vim.keymap.set('n', 'gy', vim.lsp.buf.declaration, opts)

vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)

vim.keymap.set('n', '<F4>', vim.lsp.buf.rename, opts)
vim.keymap.set('n', '<F7>', vim.lsp.buf.code_action, opts)

-- Diagnostics
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<leader>ge', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<leader>gE', vim.diagnostic.goto_prev, opts)
