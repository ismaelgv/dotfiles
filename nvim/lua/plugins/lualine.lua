return {
    "nvim-lualine/lualine.nvim",
    opts = {
        sections = {
            lualine_c = { {
                'filename',
                path = 1,
            } }
        },
        tabline = {
            lualine_b = { {
                'buffers',
                show_filename_only = false,
                symbols = { alternate_file = '' },
                max_length = vim.o.columns,
                buffers_color = {
                    active = function()
                        return vim.bo.modified and { fg = "#A0B0E0", bg = "252535" } or 'lualine_b_normal'
                    end,
                    inactive = { fg = '#555555', bg = '#1A1A20' }
                }
            } },
        }
    }
}
