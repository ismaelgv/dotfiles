return {
    "lewis6991/gitsigns.nvim",
    opts = {
        signcolumn = false,
        numhl = true,
    },
    config = function(_, opts)
        local gitsigns = require("gitsigns")
        gitsigns.setup(opts)

        vim.keymap.set("n", "<leader>gg", gitsigns.next_hunk)
        vim.keymap.set("n", "<leader>gG", gitsigns.prev_hunk)
        vim.keymap.set("n", "<leader>ga", gitsigns.stage_hunk)
        vim.keymap.set("n", "<leader>gr", gitsigns.reset_hunk)
        vim.keymap.set("n", "<leader>gp", gitsigns.preview_hunk_inline)
        vim.keymap.set("n", "<leader>gb", gitsigns.blame_line)
    end
}
